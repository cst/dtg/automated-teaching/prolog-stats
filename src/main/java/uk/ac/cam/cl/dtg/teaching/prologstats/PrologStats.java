package uk.ac.cam.cl.dtg.teaching.prologstats;

import com.igormaznitsa.prologparser.DefaultParserContext;
import com.igormaznitsa.prologparser.GenericPrologParser;
import com.igormaznitsa.prologparser.ParserContext;
import com.igormaznitsa.prologparser.PrologParser;
import com.igormaznitsa.prologparser.tokenizer.Op;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class PrologStats {
  private static int rules = 0;
  private static int queries = 0;
  private static int facts = 0;

  public static void main(String[] args) throws IOException {
    try (Reader r = new FileReader(args[0])) {
      PrologParser parser =
          new GenericPrologParser(
              r, DefaultParserContext.of(ParserContext.FLAG_CURLY_BRACKETS, Op.SWI));
      parser.forEach(
          t -> {
            if (t.getFunctor().getText().equals(":-")) {
              if (t.getFunctor().getArity() == 1) {
                queries++;
              } else {
                rules++;
              }
            } else {
              facts++;
            }
          });
      System.out.printf("{\"facts\":%d,\"rules\":%d,\"queries\":%d}%n", facts, rules, queries);
    }
  }
}
